# Media Volume Control
## A Personal Chrome Extension.
This is a chrome extension to control the volume of video and audio.
The extension syncs all pages audio and video to the value of the extension and stores the value permanently into storage.
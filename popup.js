let volumeInput = document.getElementById("volume");
let header = document.getElementById("volume-value");

const debouncedonVolumeChanged = debounce(onVolumeChanged, 1000);

chrome.storage.sync.get("media-volume", result => {
    let volume = result["media-volume"];
    if (volume === undefined) return;
    console.info("Volume is to " + volume);
    setVisualValues(volume);
});

function setVisualValues(value) {
    volume = (value * 100).toFixed(0);
    volumeInput.value = volume;
    header.innerHTML = volume + "%";
}

function onVolumeValueChanged(e) {
    newVolume = e.target.value;
    header.innerHTML = newVolume + "%";

    debouncedonVolumeChanged(newVolume);
}

function onVolumeChanged(value) {
    value /= 100;

    chrome.storage.sync.set({ "media-volume": value }, function() {
        console.info("Volume is set to " + value);
    });
}

volumeInput.oninput = onVolumeValueChanged;

function debounce(func, wait, immediate) {
    let timeout;
    return function() {
        const context = this,
            args = arguments;
        const callNow = immediate && !timeout;
        clearTimeout(timeout);
        timeout = setTimeout(function() {
            timeout = null;
            if (!immediate) {
                func.apply(context, args);
            }
        }, wait);
        if (callNow) func.apply(context, args);
    };
}

let volume;
let mediaWhichIsPlaying;

chrome.storage.onChanged.addListener(function (changes, namespace) {
    const mediaVolume = changes["media-volume"];
    if (mediaVolume === undefined) return;

    volume = mediaVolume.newValue;
    if (mediaWhichIsPlaying) mediaWhichIsPlaying.volume = volume;
});

chrome.storage.sync.get("media-volume", function (result) {
    const mediaVolume = result["media-volume"];
    console.info("Volume is to " + mediaVolume);
    volume = mediaVolume;
});

const debounce = (func, wait, immediate) => {
    let timeout;
    return function () {
        const context = this,
            args = arguments;
        const callNow = immediate && !timeout;
        clearTimeout(timeout);
        timeout = setTimeout(function () {
            timeout = null;
            if (!immediate) {
                func.apply(context, args);
            }
        }, wait);
        if (callNow) func.apply(context, args);
    };
};

const onVolumeChanged = (e) => {
    e.target.volume = volume;
};
const debouncedonVolumeChanged = debounce(onVolumeChanged, 1000);

const onMediaPlay = (e) => {
    mediaWhichIsPlaying = e.target;
    if (volume !== undefined) e.target.volume = volume;
};

const findAndBindToMedias = () => {
    let allVideos = document.getElementsByTagName("video");
    let allAudios = document.getElementsByTagName("audio");
    let medaiElements = [...allVideos, ...allAudios];

    for (let i = 0; i < medaiElements.length; i++) {
        const medaiElement = medaiElements[i];
        medaiElement.onvolumechange = debouncedonVolumeChanged;
        medaiElement.onplay = onMediaPlay;
    }
};
findAndBindToMedias();

const onNewContentAdded = () => {
    findAndBindToMedias();
};
document
    .getElementsByTagName("body")[0]
    .addEventListener("DOMNodeInserted", onNewContentAdded);
